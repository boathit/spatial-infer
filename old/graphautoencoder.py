import warnings
warnings.filterwarnings("ignore")
import torch
import torch.nn.functional as F
import numpy as np
import pandas as pd
import os
from torch_geometric.data import Data, DataLoader
from torch_geometric.utils import normalized_cut
from torch_geometric.nn import (ChebConv, graclus, GCNConv, 
                                max_pool, max_pool_x, global_mean_pool)
from toolz.curried import *
from utils import *

from pandas import DataFrame
from networkx import Graph

def get_x(df: DataFrame, num_nodes: int) -> torch.FloatTensor:
    """
    Get pytorch geometric input feature from observation dataframe.

    Inputs
        df: The observation dataframe with edgeid being attached. 
    Returns
        x (num_nodes, num_features): Input feature tensor. 
    """
    node_obs = {u: [v] for (u, v) in zip(df.edgeid.values, df.speed_mph_mean.values)}
    ## (num_nodes, 1)
    return torch.FloatTensor([get(u, node_obs, [0]) for u in range(num_nodes)])

def get_data(G: Graph, obs: DataFrame, unobs: DataFrame) -> Data:
    edge_index = get_edge_index(G)
    x = get_x(obs, G.number_of_nodes())
    y = get_x(unobs, G.number_of_nodes())
    return Data(x=x, edge_index=edge_index, y=y)

def normalized_cut_2d(edge_index: torch.LongTensor, num_nodes: int) -> torch.FloatTensor:
    edge_attr = torch.ones(edge_index.shape[1], device=edge_index.device)
    return normalized_cut(edge_index, edge_attr, num_nodes=num_nodes)

class ChebNet(torch.nn.Module):
    def __init__(self, num_features, num_nodes):
        super(ChebNet, self).__init__()
        self.conv1 = ChebConv(num_features, 32, 2)
        self.conv2 = ChebConv(32, 64, 2)
        self.fc1 = torch.nn.Linear(64, 128)
        self.fc2 = torch.nn.Linear(128, num_nodes)
    
    def forward(self, data):
        x = F.relu(self.conv1(data.x, data.edge_index))
        cluster = graclus(data.edge_index, num_nodes=x.shape[0])
        data = max_pool(cluster, Data(x=x, batch=data.batch, edge_index=data.edge_index))
        
        x = F.relu(self.conv2(data.x, data.edge_index))
        cluster = graclus(data.edge_index, num_nodes=x.shape[0])
        x, batch = max_pool_x(cluster, x, data.batch)

        x = global_mean_pool(x, batch)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return x

class GCNNet(torch.nn.Module):
    def __init__(self, num_features, num_nodes):
        super(GCNNet, self).__init__()
        #self.conv1 = ChebConv(num_features, 32, 2)
        #self.conv2 = ChebConv(32, 64, 2)
        self.conv1 = GCNConv(num_features, 32)
        self.conv2 = GCNConv(32, 64)
        self.fc1 = torch.nn.Linear(64, 128)
        self.fc2 = torch.nn.Linear(128, num_nodes)

    def forward(self, data):
        x = F.relu(self.conv1(data.x, data.edge_index))
        x = F.relu(self.conv2(x, data.edge_index))

        x = global_mean_pool(x, data.batch)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return x

def train(epoch, optimizer, train_loader, model, device):
    model.train()
    losses = []
    for data in train_loader:
        data = data.to(device)
        optimizer.zero_grad()
        xhat = model(data)
        ## -> (batch_size, num_nodes)
        x = data.x.reshape(xhat.shape)
        nz = x > 0
        loss = F.mse_loss(xhat[nz], x[nz], reduction='sum') / nz.sum().item()
        loss.backward()
        optimizer.step()

        losses.append(loss.item())
    print(f"Epoch is {epoch}, Training Loss is {np.mean(losses):.5f}")

@torch.no_grad()
def test(test_loader, model, device):
    model.eval()
    test_stats = pd.DataFrame({})
    for i, data in enumerate(test_loader):
        data = data.to(device)
        xhat = model(data)
        y = data.y.reshape(xhat.shape)

        nz = y > 0
        y_pred = xhat[nz].cpu().numpy()
        y_test = y[nz].cpu().numpy()
        edgeid = np.argwhere(nz.cpu().numpy().ravel()).ravel()

        test_stats = collect_test_stats(test_stats, i, edgeid, y_test, y_pred)
        if i % 10 == 0:
            print(f"Snapshot {i:4d}" + evaluated_message(y_test, y_pred))

    return test_stats


np.random.seed(123)
torch.manual_seed(123)
uberdir = "/home/xiucheng/data/uber-movement/"
nykjuly = os.path.join(uberdir, "movement-speeds-hourly-new-york-2019-7.csv.zip")
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

if __name__ == "__main__":
    nodes, edges = load_gdfs("data/newyork/")
    df = pd.read_csv(nykjuly)
    df = attach_edgeid(nodes, edges, df)
    dG = edge_topology_from_edges(edges)
    n_test = 3 * 24
    n_epochs = 5
    batch_size = 1

    obs, unobs = split_obs_unobs(df, ratio=0.9)
    obs = [g for (_, g) in obs.groupby(['month', 'day', 'hour'])]
    unobs = [g for (_, g) in unobs.groupby(['month', 'day', 'hour'])]
    trn_list = [get_data(dG, o, u) for (o, u) in zip(obs[:-n_test], unobs[:-n_test])]
    tst_list = [get_data(dG, o, u) for (o, u) in zip(obs[-n_test:], unobs[-n_test:])]
    trn_loader = DataLoader(trn_list, batch_size=batch_size)
    tst_loader = DataLoader(tst_list, batch_size=batch_size)
    model = ChebNet(1, dG.number_of_nodes()).to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

    for epoch in range(n_epochs):
        train(epoch, optimizer, trn_loader, model, device)
    
    test_stats = test(tst_loader, model, device)
    save_test_stats(test_stats, "data/exp", "ga_test_stats")
    print(evaluated_message(test_stats.y_test.values, test_stats.y_pred.values))
