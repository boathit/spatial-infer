import warnings
warnings.filterwarnings("ignore")
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import pandas as pd
import os
from toolz.curried import *
from utils import *
from torch import Tensor, LongTensor, FloatTensor
from torch_geometric.nn import GCNConv, ChebConv, ARMAConv
from typing import Optional

class GCNNet(torch.nn.Module):
    def __init__(self, dim_in: int, dim_out: int, num_nodes: int):
        super(GCNNet, self).__init__()
        self.conv1 = GCNConv(dim_in, dim_in)
        self.conv2 = ChebConv(dim_in, dim_in, 2)
        self.conv3 = ARMAConv(dim_in, dim_in, num_stacks=1, num_layers=1)
        self.fc1 = nn.Linear(dim_in, dim_out)

    def forward(self, x: FloatTensor, edge_index: LongTensor):
        z = F.relu(self.conv3(x, edge_index))
        z = self.fc1(z)
        return z

class GCNMF(nn.Module):
    def __init__(self, N: int, T: int, dim_size: int, num_nodes: int):
        super(GCNMF, self).__init__()
        self.s_embeddings = nn.Embedding(N, 100)
        self.t_embeddings = nn.Embedding(T, dim_size)
        self.dims = (N, T)
        self.gcn = GCNNet(100, dim_size, num_nodes)

    def _forward(self, roads, times, edge_index):
        """
        roads (batch_size,): road link ids.
        times (batch_size,): time steps.
        """
        N, device = self.dims[0], self.get_device
        U = self.getU(torch.arange(N).to(device))
        U = self.gcn(U, edge_index)
        ## (batch_size, dim_size)
        ms = U[roads]
        ## (T, dim_size)
        mt = self.t_embeddings(times)
        ## (batch_size, T)                                            
        return torch.mm(ms, mt.t())

    def forward(self, roads, edge_index):
        """
        roads (batch_size, ): road link ids.
        """
        T = self.dims[1]
        times = torch.arange(T,  dtype=roads.dtype, device=roads.device)
        return self._forward(roads, times, edge_index)

    def infer(self, edge_index):
        """
        Infer the values of last time step.
        """
        N, T = self.dims
        device = self.get_device

        roads = torch.arange(N).to(device)
        times = torch.LongTensor([T-1]).to(device)
        return self._forward(roads, times, edge_index)

    @property
    def get_device(self):
        return self.s_embeddings.weight.device

    def getU(self, roads) -> FloatTensor:
        return self.s_embeddings(roads)

def obsLoss(model, X, roads, edge_index):
    """
    X (N, T)
    roads (batch_size,)
    """
    ## (batch_size, T)
    x = X[roads]
    device = model.get_device

    x, roads = x.to(device), roads.to(device)
    xhat = model(roads, edge_index)
    return obsLossF(xhat, x)

def train(
    optimizer: torch.optim.Optimizer, 
    num_epochs: int, 
    batch_size: int, 
    model: nn.Module, 
    X: Tensor,
    edge_index: LongTensor,
    verbose: bool
):
    def train_step(i, roads):
        loss = obsLoss(model, X, roads, edge_index)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        return loss.item()
    
    def train_epoch(epoch, idx):
        model.train()

        np.random.shuffle(idx)
        N, running_losses = len(idx), []
        for st_idx in range(0, N, batch_size):
            end_idx = min(st_idx + batch_size, N)
            roads = torch.LongTensor(idx[st_idx:end_idx])
            running_losses.append(train_step(st_idx//batch_size, roads))
        return np.mean(running_losses)
    
    idx = np.arange(X.shape[0])
    for epoch in range(num_epochs):
        running_loss = train_epoch(epoch, idx)
        if verbose and (epoch + 1) % 100 == 0:
            print(f"Epoch:{epoch:3d} running loss = {running_loss:10.4f}")
        
@torch.no_grad()
def test(model, X_unobs, edge_index):
    model.eval()

    xhat = model.infer(edge_index)
    x = X_unobs[:, -1:].to(xhat.device)
    mask = torch.logical_not(x.isnan())

    y_test, y_pred = x[mask], xhat[mask]
    edgeid, _ = torch.where(mask)
    return y_test.cpu().numpy(), y_pred.cpu().numpy(), edgeid.cpu().numpy()

def one_snapshot(
    X_obs: Tensor,
    X_unobs: Tensor,
    edge_index: LongTensor,
    num_epochs: int, 
    batch_size: int, 
    dim_size: int,
    num_nodes: int,
    device: torch.device,
    verbose: Optional[bool] = False
):
    edge_index = edge_index.to(device)
    model = GCNMF(X_obs.shape[0], X_obs.shape[1], dim_size, num_nodes).to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001, weight_decay=1e-4)
    train(optimizer, num_epochs, batch_size, model, X_obs, edge_index, verbose)
    return test(model, X_unobs, edge_index)

np.random.seed(123)
torch.manual_seed(123)
uberdir = "/home/xiucheng/data/uber-movement/"
nykjuly = os.path.join(uberdir, "movement-speeds-hourly-new-york-2019-7.csv.zip")
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    
if __name__ == "__main__":
    nodes, edges = load_gdfs("data/newyork/")
    df = pd.read_csv(nykjuly)
    df = attach_edgeid(nodes, edges, df)
    dG = edge_topology_from_edges(edges)

    n_test = 2 * 24
    num_epochs = 500
    batch_size = 256
    dim_size = 100
    num_nodes = dG.number_of_nodes()
    edge_index = get_edge_index(dG)

    obs, unobs = split_obs_unobs(df, ratio=0.9)
    xa_obs = dataframe2xarray(obs, edges.shape[0])
    xa_unobs = dataframe2xarray(unobs, edges.shape[0])
    X_obs, X_unobs = torch.FloatTensor(xa_obs.values), torch.FloatTensor(xa_unobs.values)
    test_stats = pd.DataFrame({})
    for i in range(n_test):
        y_test, y_pred, edgeid = one_snapshot(
            X_obs[:, :-n_test+i], 
            X_unobs[:, :-n_test+i],
            edge_index,
            num_epochs, 
            batch_size, 
            dim_size, 
            num_nodes,
            device
        )

        test_stats = collect_test_stats(test_stats, i, edgeid, y_test, y_pred)
        if i % 10 == 0:
            print(f"Snapshot {i:4d}" + evaluated_message(y_test, y_pred))
    save_test_stats(test_stats, "data/exp", "gcnmf_test_stats")
    print(evaluated_message(test_stats.y_test.values, test_stats.y_pred.values))                      
                                
                                






    