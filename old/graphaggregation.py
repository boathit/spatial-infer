import warnings
warnings.filterwarnings("ignore")
import numpy as np
import pandas as pd
import geopandas as gpd
import networkx as nx
import os
from toolz.curried import *
from utils import *
from sklearn import metrics
from tqdm import tqdm

from pandas import DataFrame, Series
from networkx import Graph

def attach_observation(G: Graph, df: DataFrame) -> Graph:
    """
    Attaching observation to the edge topology graph.

    Inputs
        G: The edge topology graph.
        df: The observation dataframe with edgeid being attached. 
    
    Returns
        G: An undirected graph with "obs" node attribute.

    Example
        G = edge_topology_from_edges(edges)
        G = attach_observation(G, hf_train)
    """
    nx.set_node_attributes(G, -1.0, "obs")
    for (edgeid, val) in zip(df.edgeid.values, df.speed_mph_mean.values):
        G.nodes[edgeid]["obs"] = val
    return G

def get_observation(G: Graph) -> Series:
    node_obs = {node: obs for (node, obs) in G.nodes(data="obs")}
    return Series(node_obs)

def aggregate_observation(G: Graph) -> Graph:
    """
    Aggregating observation over the graph.

    Inputs
        Graph with partial observation.
    Returns
        Graph updated by the aggregation.
    """
    def aggregate_neighbors(G: Graph, node: int) -> float:
        obs = [G.nodes[n]["obs"] for n in G.neighbors(node) if G.nodes[n]["obs"] >= 0]
        return np.mean(obs) if len(obs) > 0 else -1.0
    ## need to cache the update
    node_obs = [(node, aggregate_neighbors(G, node)) \
                for (node, obs) in G.nodes(data="obs") if obs < 0]
    ## update graph using the cache
    for (node, obs) in node_obs:
        G.nodes[node]["obs"] = obs
    return G

def progressive_aggregatation(G: Graph, max_iter=10) -> Series:
    for _ in range(max_iter):
        G = aggregate_observation(G)
        o = get_observation(G)
        if np.sum(o > 0) == o.shape[0]: break
    return get_observation(G)

np.random.seed(123)
uberdir = "/home/xiucheng/data/uber-movement/"
nykjuly = os.path.join(uberdir, "movement-speeds-hourly-new-york-2019-7.csv.zip")

if __name__ == "__main__":
    nodes, edges = load_gdfs("data/newyork/")
    df = pd.read_csv(nykjuly)
    df = attach_edgeid(nodes, edges, df)
    dG = edge_topology_from_edges(edges)

    # hf = pipe(df.groupby(['day', 'hour']), first, second)
    # hf_train, hf_test = split_dataframe(hf, 0.8)
    obs, unobs = split_obs_unobs(df)
    obs_list = [g for (_, g) in obs.groupby(['month', 'day', 'hour'])]
    unobs_list = [g for (_, g) in unobs.groupby(['month', 'day', 'hour'])]
    n_test = 3 * 24
    test_stats = pd.DataFrame({})
    for i, (hf_train, hf_test) in enumerate(zip(obs_list[-n_test:], unobs_list[-n_test:])):
        dG = attach_observation(dG, hf_train)
        result = progressive_aggregatation(dG)
    
        y_test = hf_test.speed_mph_mean.values
        y_pred = result[hf_test.edgeid].values
        edgeid = hf_test.edgeid.values

        test_stats = collect_test_stats(test_stats, i, edgeid, y_test, y_pred)
        if i % 10 == 0:
            print(f"Snapshot {i:4d}" + evaluated_message(y_test, y_pred))
    
    save_test_stats(test_stats, "data/exp", "pa_test_stats")
    print(evaluated_message(test_stats.y_test.values, test_stats.y_pred.values))