
import torch
import torch.nn as nn
import torch.nn.functional as F

from torch_geometric.nn import MessagePassing
from torch import Tensor, LongTensor
from toolz.curried import *
from typing import Callable, Optional, Tuple, Any

import numpy as np
import pandas as pd
import os
from utils import *

class NodeEdgeConv(MessagePassing):
    def __init__(self, in_channels: int, out_channels: int, edge_channels: int) -> None:
        super(NodeEdgeConv, self).__init__(aggr="mean")
        self.fc_m = nn.Linear(in_channels + edge_channels, out_channels)
        self.fc_h = nn.Linear(out_channels + in_channels, out_channels)
        self.σ = nn.ReLU()

    def forward(self, x: Tensor, edge_attr: Tensor, edge_index: LongTensor) -> Tensor:
        """
        Args:
            x (N, in_channels): node features.
            edge_attr (E, edge_channels): edge features.
            edge_index (2, E)
        Returns:
            (N, out_channels)
        """
        N = x.shape[0]
        return self.propagate(edge_index, x=x, edge_attr=edge_attr, size=(N, N))

    def message(self, x_j: Tensor, edge_attr: Tensor) -> Tensor:
        """
        The message from source nodes.

        Args:
            x_j (E, in_channels): source node features.
            edge_attr (E, edge_channels): edge features.
        Returns:
            (E, out_channels)
        """
        return pipe(torch.cat([x_j, edge_attr], dim=-1), self.fc_m, self.σ)
                    
    
    def update(self, aggregation: Tensor, x: Tensor) -> Tensor:
        """
        Produce the output by combining the present node representation `x` and message
        aggregated from neighbors `aggregation`.

        Args:
            aggregation (N, out_channels): the message aggregated from neighbors.
            x (N, in_channels)
        Returns:
            (N, out_channels)
        """
        aggregation = F.normalize(aggregation, p=2, dim=-1)
        return pipe(torch.cat([aggregation, x], dim=-1), self.fc_h, self.σ)                    


class BipartiteNet(nn.Module):
    def __init__(
        self,
        node_dim0: int, 
        edge_dim0: int,
        node_dim:  int, 
        edge_dim:  int,
        num_layer: int,
        hidden_dim: int
    ) -> None:
        super(BipartiteNet, self).__init__()
        self.σ = nn.ReLU()

        self.convs = \
            self.create_convs(node_dim0, edge_dim0, node_dim, edge_dim, num_layer)
        self.edge_σs = \
            self.create_edge_σs(self.σ, edge_dim0, node_dim, edge_dim, num_layer)
        ## last layer node transformation
        self.node_f = nn.Sequential(
            nn.Linear(node_dim, hidden_dim),
            self.σ,
            nn.Dropout(),
            nn.Linear(hidden_dim, node_dim)
        ) if hidden_dim > 0 else lambda x: x
    
    def forward(self, x: Tensor, edge_attr: Tensor, edge_index: LongTensor) -> Tensor:
        """
        conv_0 -> update_edge_0 -> conv_i -> update_edge_i... -> single_node_map
        Args:
            x (N, node_dim0): node features.
            edge_attr (E, edge_channels): edge features.
            edge_index (2, E)
        Returns:
            (N, node_dim)
        """
        def update_edge(x, edge_attr, edge_index, edge_σ):
            x_i, x_j = x[edge_index[0]], x[edge_index[1]]
            return edge_σ(torch.cat([x_i, x_j, edge_attr], dim=-1))
        
        for conv, edge_σ in zip(self.convs, self.edge_σs):
            ## update nodes
            x = conv(x, edge_attr, edge_index)
            ## update edges
            edge_attr = update_edge(x, edge_attr, edge_index, edge_σ)
        return self.node_f(x)

    # @staticmethod
    # def update_edge(
    #     x: Tensor, 
    #     edge_attr: Tensor, 
    #     edge_index: LongTensor, 
    #     edge_σ: Callable
    # ) -> Tensor:
    #     x_i, x_j = x[edge_index[0, :]], x[edge_index[1, :]]
    #     return edge_σ(torch.cat([x_i, x_j, edge_attr], dim=-1))

    @staticmethod
    def create_convs(
        node_dim0: int, 
        edge_dim0: int,
        node_dim:  int, 
        edge_dim:  int,
        num_layer: int 
    ) -> nn.ModuleList:
        conv_list = [NodeEdgeConv(node_dim0, node_dim, edge_dim0)] + \
            [NodeEdgeConv(node_dim, node_dim, edge_dim) for _ in range(num_layer-1)]
        return nn.ModuleList(conv_list)
    
    @staticmethod
    def create_edge_σs(
        σ: Callable,
        edge_dim0: int,
        node_dim:  int, 
        edge_dim:  int,
        num_layer: int
    ) -> nn.ModuleList:
        edge_σ_list = [nn.Sequential(nn.Linear(node_dim+node_dim+edge_dim0, edge_dim), σ)] + \
            [nn.Sequential(nn.Linear(node_dim+node_dim+edge_dim, edge_dim), σ)
                for _ in range(num_layer-1)]
        return nn.ModuleList(edge_σ_list)
        
        
class BiImputation(nn.Module):
    def __init__(
        self,
        node_dim0: int, 
        edge_dim0: int,
        node_dim:  int, 
        edge_dim:  int,
        num_layer: int,
        hidden_dim1: int,
        hidden_dim2: int,
        output_dim: Optional[int]=1,
        σ: Optional[Callable]=nn.ReLU()
    ) -> None:
        super(BiImputation, self).__init__()

        self.bipartitenet = \
            BipartiteNet(node_dim0, edge_dim0, node_dim, edge_dim, num_layer, hidden_dim1)
        self.mlp = nn.Sequential(
            nn.Linear(node_dim+node_dim, hidden_dim2),
            σ,
            nn.Dropout(),
            nn.Linear(hidden_dim2, output_dim)
        )
    
    def forward(
        self, 
        x: Tensor, 
        edge_attr: Tensor, 
        edge_index: LongTensor,
        edge_index_p: LongTensor,
    ) -> Tensor:
        """
        Args:
            x (N, in_channels): node features.
            edge_attr (E, edge_channels): edge features.
            edge_index (2, E)
            edge_index_p (2, E_p): the edge set to be predicted (imputed).
        Returns:
            (E_p, out_channels)
        """
        ## (N, node_dim0) => (N, node_dim)
        x = self.bipartitenet(x, edge_attr, edge_index)
        ## => (E_p, node_dim)
        x_i, x_j = x[edge_index_p[0]], x[edge_index_p[1]]
        ## (E_p, node_dim) => (E_p, output_dim)
        return self.mlp(torch.cat([x_i, x_j], -1))

def get_bipartite_edges(X: Tensor) -> Tuple[Tensor, LongTensor]:
    """
    Args:
        X (n, m, dim): the spatial-temporal tensor.
    Returns:
        edge_attr (E, dim): the (observed) edge features.
        edge_index (2, E)
    """
    X0, n = X[:, :, 0], X.shape[0]
    observed = torch.logical_not(X0.isnan())
    edge_attr =  X[observed]
    edge_index = torch.nonzero(observed).t().contiguous()
    edge_index[1] = edge_index[1] + n
    # undirected graph
    edge_index = torch.cat([edge_index, torch.flipud(edge_index)], dim=-1)
    edge_attr = torch.cat([edge_attr, edge_attr], dim=0)
    return edge_attr, edge_index

def drop_edges(drop_rate, edge_attr: Tensor, edge_index: LongTensor):
    """
    Args:
        edge_attr (E, dim): the (observed) edge features.
        edge_index (2, E)
    """
    E = edge_attr.shape[0] // 2
    # n = int(E * (1 - drop_rate))
    # index = torch.randperm(E, device=edge_attr.device)[:n]
    index = torch.rand(E, device=edge_attr.device) >= drop_rate
    ## undirected graph
    index = torch.cat([index, index])
    return edge_attr[index, :], edge_index[:, index]
    

def get_bipartie_nodes(n: int, m: int) -> Tensor:
    """
    Args:
        n: the number of spatial objects.
        m: the number of time steps.
    Returns:
        x (n+m, m): the initialized node features.
    """
    return torch.cat([torch.ones(n, m), torch.eye(m)], dim=0)

def train(
    X: Tensor, 
    num_epochs: int, 
    node_dim: Optional[int]=64, 
    edge_dim: Optional[int]=64, 
    num_layer: Optional[int]=3, 
    hidden_dim1: Optional[int]=0,
    hidden_dim2: Optional[int]=100,
    drop_rate: Optional[float]=0.7,
    verbose: Optional[bool]=False,
    device: Optional[torch.device]=torch.device("cpu")
) -> Any:
    edge_attr_f, edge_index_f = get_bipartite_edges(X)
    scaler, edge_attr_f = minmaxscaler(edge_attr_f)
    edge_attr_f, edge_index_f = edge_attr_f.to(device), edge_index_f.to(device)
    x = get_bipartie_nodes(X.shape[0], X.shape[1]).to(device)

    E_f = edge_attr_f.shape[0] // 2
    edge_attr_p, edge_index_p = edge_attr_f[:E_f, :], edge_index_f[:, :E_f]
    node_dim0 = x.shape[-1]
    edge_dim0 = X.shape[-1]

    model = BiImputation(node_dim0, edge_dim0, node_dim, edge_dim,
                         num_layer, hidden_dim1, hidden_dim2).to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

    def train_step(edge_attr, edge_index):
        model.train()
        z = model(x, edge_attr, edge_index, edge_index_p)
        loss = F.mse_loss(z, edge_attr_p)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        return loss.item()
    
    for epoch in range(num_epochs):
        edge_attr, edge_index = drop_edges(drop_rate, edge_attr_f, edge_index_f)
        loss = train_step(edge_attr, edge_index)
        if verbose and (epoch+1) % 200 == 0:
            print(f"Epoch:{epoch:3d} loss = {loss:10.6f}")
    
    return model, scaler, (x, edge_attr, edge_index, edge_index_p), edge_attr_p

np.random.seed(123)
torch.manual_seed(123)
uberdir = "/home/xiucheng/data/uber-movement/"
nykjuly = os.path.join(uberdir, "movement-speeds-hourly-new-york-2019-7.csv.zip")
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    
if __name__ == "__main__":
    nodes, edges = load_gdfs("data/newyork/")
    df = pd.read_csv(nykjuly)
    df = attach_edgeid(nodes, edges, df)
    
    n_test = 3 * 24
    num_epochs = 500

    obs, unobs = split_obs_unobs(df, ratio=0.9)
    xa_obs = dataframe2xarray(obs, edges.shape[0])
    xa_unobs = dataframe2xarray(unobs, edges.shape[0])
    X_obs, X_unobs = torch.FloatTensor(xa_obs.values), torch.FloatTensor(xa_unobs.values)
    X_o, X_u = X_obs.unsqueeze(-1), X_unobs.unsqueeze(-1)
    test_stats = pd.DataFrame({})

