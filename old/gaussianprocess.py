import warnings
warnings.filterwarnings("ignore")
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF
import numpy as np
import geopandas as gpd
import pandas as pd
import os
from toolz.curried import *
from utils import *
from sklearn import metrics
from tqdm import tqdm


def gpfit(X, y, length_scale=1.0, length_scale_bounds=(1e-2, 1e3), alpha=0.1):
    kernel = RBF(length_scale=length_scale, length_scale_bounds=length_scale_bounds)
    gp = GaussianProcessRegressor(kernel=kernel, alpha=alpha).fit(X, y)
    return gp

np.random.seed(123)
uberdir = "/home/xiucheng/data/uber-movement/"
nykjuly = os.path.join(uberdir, "movement-speeds-hourly-new-york-2019-7.csv.zip")

if __name__ == "__main__":
    nodes, edges = load_gdfs("data/newyork/")
    df = pd.read_csv(nykjuly)
    df = attach_edgeid(nodes, edges, df)

    obs, unobs = split_obs_unobs(df)
    obs_list = [g for (_, g) in obs.groupby(['month', 'day', 'hour'])]
    unobs_list = [g for (_, g) in unobs.groupby(['month', 'day', 'hour'])]
    # using last 3 days as testing data
    n_test = 3 * 24
    test_stats = pd.DataFrame({})
    for i, (hf_train, hf_test) in enumerate(zip(obs_list[-n_test:], unobs_list[-n_test:])):
        X_train = get_coords(edges, hf_train.edgeid.values)
        y_train = hf_train.speed_mph_mean.values
        X_test = get_coords(edges, hf_test.edgeid.values)
        gp = gpfit(X_train, y_train, length_scale=10., alpha=1.0)
        
        y_pred = gp.predict(X_test)
        y_test = hf_test.speed_mph_mean.values
        edgeid = hf_test.edgeid.values
    
        test_stats = collect_test_stats(test_stats, i, edgeid, y_test, y_pred)
        if i % 10 == 0:
            print(f"Snapshot {i:4d}" + evaluated_message(y_test, y_pred))
    
    save_test_stats(test_stats, "data/exp", "gp_test_stats")
    print(evaluated_message(test_stats.y_test.values, test_stats.y_pred.values))
