import pandas as pd
import numpy as np
import osmnx as ox
import networkx as nx
import torch, os
import torch.nn.functional as F
import xarray as xa
import geopandas as gpd

from datetime import datetime
from toolz.curried import *
from typing import List, Tuple, Optional
from pandas import DataFrame
from geopandas import GeoDataFrame
from networkx import MultiDiGraph, Graph
from sklearn import metrics
from sklearn.preprocessing import MinMaxScaler
from torch import Tensor

def save_graphml_from_places(place_names: List[str], datadir: str) -> None:
    """
    Download the road networks from place names and saving
    the graphs as graphml into `datadir` folder.

    Usage:
        place_names = ['Manhattan, New York, USA']
        save_graphml_from_places(place_names, "data/newyork/")
    """
    G = ox.graph_from_place(place_names, network_type='drive')
    if not os.path.exists(datadir): os.mkdir(datadir)
    ox.save_graphml(G, os.path.join(datadir, "graph.graphml"))

def load_gdfs(datadir: str) -> Tuple[GeoDataFrame, GeoDataFrame]:
    """
    Load the nodes and edges GeoDataFrame from graphml
    saved by `save_graphml_from_places`.

    Usage:
        nodes, edges = load_gdfs("data/newyork/")
    """
    G = ox.load_graphml(os.path.join(datadir, "graph.graphml"))

    nodes, edges = ox.graph_to_gdfs(G, nodes=True, edges=True)
    nodes['osmid'] = nodes.index.values
    ## the centroid coordindates of road segments
    points = edges.geometry.to_crs(epsg=3395).centroid
    coords = pipe(points.map(lambda p: (p.x, p.y)).values, map(list), list, np.array)
    coords = coords - coords.min(axis=0)

    edges['coords'] = pipe(coords, map(tuple), list)
    edges['osmid'] = edges.osmid.map(lambda x: x if isinstance(x, list) else [x])
    u, v, _ = list(zip(*edges.index))
    edges["u"] = u
    edges["v"] = v
    edges['id'] = np.arange(edges.shape[0])
    edges.set_index('id', inplace=True, drop=False)
    print(f"There are {nodes.shape[0]} nodes and {edges.shape[0]} edges in the road networks.")
    
    return nodes, edges

def edge_topology_from_edges(edges: GeoDataFrame) -> Graph:
    """
    Construct edge topology from the `edges` (The graph with road segments as nodes).

    nx.line_graph() can construct the line graph directly from the original graph.

    Args
        edges: Geodataframe returned by load_gdfs.
    Returns
        G: A undirected graph whose node ids are edge ids in `edges`.
    """
    triple = pd.concat([pd.DataFrame({'id': edges.id, 'u': edges.u, 'v': edges.v}),
                        pd.DataFrame({'id': edges.id, 'u': edges.v, 'v': edges.u})],
                       ignore_index=True)
    pairs = []
    for (_, g) in triple.groupby('u'):
        pairs += [(u, v) for u in g.id for v in g.id if u != v]
    for (_, g) in triple.groupby('v'):
        pairs += [(u, v) for u in g.id for v in g.id if u != v]
    G = Graph()
    G.add_edges_from(pairs)
    return G

def torch_sparse_laplacian(G: Graph) -> torch.sparse.FloatTensor:
    """
    Given a networkx graph `G`, return its laplacian matrix in pytorch sparse tensor.
    """
    L = nx.laplacian_matrix(G).tocoo()

    i = torch.LongTensor(np.stack((L.row, L.col)))
    v = torch.FloatTensor(L.data.astype(np.float32))
    return torch.sparse.FloatTensor(i, v, torch.Size(L.shape))

def get_edge_index(G: Graph) -> torch.LongTensor:
    """
    Create pytorch geometric edge_index from a networkx graph.
    """
    edge_index = pipe(G.edges(), map(list), list, torch.LongTensor)
    return edge_index.t().contiguous()

def attach_edgeid(nodes: GeoDataFrame, edges: GeoDataFrame, df: DataFrame) -> DataFrame:
    """
    Filter and attaching uber one-month dataframe `df` a graph edge-id column, 
    where the edge id is determined by (u, v, osmid) and only rows with edge id are kept.

    Usage:
        mh = attach_edgeid(nodes, edges, df)
    """
    ## filtering by node ids
    sdf = df[df.osm_start_node_id.isin(nodes.osmid)&df.osm_end_node_id.isin(nodes.osmid)].copy()
    ## dropping columns that will not be used
    sdf.drop(["segment_id", "start_junction_id", "end_junction_id"], axis=1, inplace=True)

    edgeidmap = {(u, v): (osmid, edgeid) for (u, v, osmid, edgeid) in 
                 zip(edges.u, edges.v, edges.osmid, edges.id)}
    def getedgeid(u: int, v: int, osmid: int) -> int:
        """
        Map the (u, v, osmid) tuple to the corresponding graph edge id 
        and return -1 if there is no such edge in the graph.
        """
        osmids, edgeid = get((u, v), edgeidmap, ([-1], -1))
        return edgeid if osmid in osmids else -1

    edge_idx_cols = ['osm_start_node_id', 'osm_end_node_id', 'osm_way_id']
    sdf['edgeid'] = sdf[edge_idx_cols].apply(lambda x: getedgeid(*x), axis=1)
    sdf = sdf[sdf.edgeid >= 0]
    return sdf

def dataframe2xarray(df: DataFrame, num_edges: int) -> xa.Dataset: 
    """
    Convert the uber one-month dataframe with edgeid being attached to xarray,
    a spatiotemporal matrix.

    Args
        df: One-month dataframe with edgeid being attached.
        num_edges: The number of graph edges.
    Returns
        x (num_edges, T): A spatiotemporal matrix with dimension `s` and `t`.
    """
    df['datetime'] = df[['year', 'month', 'day', 'hour']].apply(lambda x: datetime(*x), axis=1)
    df = df[['edgeid', 'datetime', 'speed_mph_mean']].set_index(['edgeid', 'datetime'])
    speed_mph_mean = pd.Series(df.speed_mph_mean, df.index, dtype=np.float32)
    x = xa.DataArray.from_series(speed_mph_mean)
    
    assert num_edges > len(x.edgeid), 'invalid num_edges'
    missed_edges = np.setdiff1d(np.arange(num_edges), x.edgeid.values)
    z = xa.DataArray(np.float32(np.nan), [('edgeid', missed_edges), 
                                          ('datetime', x.datetime.values)])
    x = xa.concat([x, z], dim='edgeid').rename({'edgeid': 's', 'datetime': 't'})
    return x.sel(s=np.arange(num_edges))                                               


def get_coords(edges: GeoDataFrame, edgeids: np.ndarray) -> np.ndarray:
    """
    Returns the coordindates of the `edgeids`.
    """
    coords = pipe(edges.coords.values, map(list), list, np.array)
    return coords[edgeids]

def plot_link(G: MultiDiGraph, nodes: GeoDataFrame, u: int, v: int, dist=300) -> None:
    """
    Plot the road link (u, v).
    """
    y, x = nodes.loc[u, ['y', 'x']].values
    bbox = ox.utils_geo.bbox_from_point(point=(y, x), dist=dist)
    fig, ax = ox.plot_graph_route(G, [u, v], bbox=bbox, route_linewidth=3, node_size=.5)


def toeplitz_matrix(n: int, max_offset: Optional[int] = 1) -> torch.sparse.FloatTensor:
    """
    Constructing temporal smooth toeplitz matrix of size (n - max_offset, n).
    """
    def diag(offset: int, val: float):
        r = np.arange(n - max_offset)
        c = r + offset
        v = np.repeat(val, n - max_offset)
        return np.stack([r, c]), v
    
    i0, v0 = diag(0, 1.0)
    i1, v1 = diag(max_offset, -1.0)
    i = torch.LongTensor(np.concatenate([i0, i1], axis=1))
    v = torch.FloatTensor(np.concatenate([v0, v1]))
    
    return torch.sparse.FloatTensor(i, v, torch.Size([n - max_offset, n]))
    
def plot_links(G: MultiDiGraph, nodes: GeoDataFrame, center_node: int, 
               links: List[Tuple], dist=300) -> None:
    """
    Plot the road links specified by `links`.
    """
    n = len(links)
    assert 1 < n <= 4, "n should be >= 2 and <= 4"
    colors = ['y', 'r', 'c', 'b']
    y, x = nodes.loc[center_node, ['y', 'x']].values
    bbox = ox.utils_geo.bbox_from_point(point=(y, x), dist=dist)
    fig, ax = ox.plot_graph_routes(G, links, bbox=bbox, route_colors=colors[:n],
                                   route_linewidth=3, node_size=.5)

def MAPE(y_test: np.ndarray, y_pred: np.ndarray) -> float:
    abs_errors = np.abs(y_pred - y_test)
    return np.mean(abs_errors / y_test)

def evaluated_message(y_test: np.ndarray, y_pred: np.ndarray) -> str:
    rmse = metrics.mean_squared_error(y_test, y_pred, squared=False)
    mae  = metrics.mean_absolute_error(y_test, y_pred)
    abs_errors = np.abs(y_test - y_pred)
    mape = np.mean(abs_errors / y_test)
    msg = (f" RMSE: {rmse:7.4} MAE: {mae:7.4}"
           f" Max Error: {np.max(abs_errors):7.4} MAEP: {mape:7.4}")
    return msg

def collect_test_stats(
    a: DataFrame,
    i: int, 
    edgeid: np.array, 
    y_test: np.array, 
    y_pred: np.array) -> DataFrame:
    """
    Collecting the test statistics into the accumulator `a` with
    columns ["timest", "edgeid", "y_test", "y_pred"].

    Args
        a: Accumulator.
        i: Timestep.
        edgeid: The studied edge ids.
        y_test: The groundtruth observation.
        y_pred: The model output.
    Returns
        a: Accumulator that merges the current step record.
    """
    assert len(edgeid) == len(y_test) == len(y_test), "Input should have the same length"
    
    x = pd.DataFrame({"timest": [i] * len(edgeid), 
                      "edgeid": edgeid,
                      "y_test": y_test, 
                      "y_pred": y_pred})
    return pd.concat([a, x], ignore_index=True)

def save_test_stats(test_stats: DataFrame, path: str, name: str) -> None:
    if not os.path.exists(path):
        os.mkdir(path)
    test_stats.to_csv(os.path.join(path, f"{name}.csv"), index=False)

def append_stats(test_stats: DataFrame) -> None:
    y_test = test_stats.y_test.values
    y_pred = test_stats.y_pred.values

    test_stats['square_error'] = np.square(y_test - y_pred)
    test_stats['abs_error'] = np.abs(y_test - y_pred)
    test_stats['abs_p_error'] = test_stats['abs_error'].values / y_test

def time_error_stats(test_stats: DataFrame) -> DataFrame:
    """
    The error statistics of each hour.

    Args
        test_stats: Returned by `collect_test_stats()`.
    Returns
        A dataframe summarizes the mean error of each hour.    
    """
    cols = ["square_error", "abs_error", "abs_p_error"]
    if not cols[0] in test_stats.columns: append_stats(test_stats)

    summary = test_stats.groupby(test_stats.timest % 24)[cols].mean()
    summary["square_error"] = summary.square_error.apply(np.sqrt)
    summary.rename(columns={"square_error": "rmse",
                            "abs_error": "mae",
                            "abs_p_error": "mape"}, inplace=True)
    return summary

def road_error_stats(test_stats: DataFrame, obs_rate: Optional[np.array] = None) -> DataFrame:
    """
    The error statistics of each road segment.

    Args
        test_stats: Returned by `collect_test_stats()`.
        obs_rate: The observation rate for each road.
    Returns
        A dataframe summarizes the mean error of each road segment.
    """
    cols = ["square_error", "abs_error", "abs_p_error"]
    if not cols[0] in test_stats.columns: append_stats(test_stats)

    summary = test_stats.groupby(["edgeid"])[cols].mean()
    summary["square_error"] = summary.square_error.apply(np.sqrt)
    summary.rename(columns={"square_error": "rmse",
                            "abs_error": "mae",
                            "abs_p_error": "mape"}, inplace=True)
    if obs_rate is not None:
        summary["obs_rate"] = obs_rate[summary.index]
    return summary

def range_errors(summary: DataFrame, step: Optional[float]=0.2) -> DataFrame:
    """
    Calculate the mean errors in the range [0, step), [step, 2step), ..., 
    up to 1.0.

    Args
        summary: Returned by road_error_stats().
    Returns
        A dataframe summarizes the errors on the specified range.
    """
    discretized_obs_rate = pd.cut(summary.obs_rate, np.arange(0, 1 + step, step))
    cols = ["rmse", "mae", "mape"]
    return summary.groupby(discretized_obs_rate)[cols].mean()
    

def split_dataframe(df: DataFrame, 
                    ratio: Optional[float]=0.9) -> Tuple[DataFrame, DataFrame]:
    """
    Split a dataframe into two parts along the row dimension by the given ratio.
    """
    k = int(df.shape[0] * ratio)
    idx = np.random.permutation(df.shape[0])
    return df.iloc[idx[:k]], df.iloc[idx[k:]]

def split_obs_unobs(df: DataFrame, 
                    ratio: Optional[float]=0.9) -> Tuple[DataFrame, DataFrame]:
    """
    Split a one-month dataframe into observed and unobserved dataframes.

    Returns
        trn: Observations for a fraction of road segments.
        tst: Ground truth for road segments to be inferred. 
    """
    ## we should guarantee the results are invariant to calling order. 
    np.random.seed(123)
    dfs = [split_dataframe(g, ratio=ratio) for (_, g) in df.groupby(['month', 'day', 'hour'])]
    trn = pd.concat(pipe(dfs, map(first), list))
    tst = pd.concat(pipe(dfs, map(second), list))
    return trn, tst

def obsLossF(Xhat: Tensor, X: Tensor):
    """
    Xhat (n, m): Inferred matrix.
    X (n, m): Partially observed matrix with nan indicating missing values.
    """
    mask = torch.logical_not(X.isnan())
    return F.mse_loss(Xhat[mask], X[mask])

def minmaxscaler(x: Tensor):
    x, device = x.cpu().numpy(), x.device
    scaler = MinMaxScaler()
    z = scaler.fit_transform(x)
    return scaler, torch.from_numpy(z).to(device)

def minmaxscaler_inverse(scaler, z: Tensor):
    z, device = z.cpu().numpy(), z.device
    x = scaler.inverse_transform(z)
    return torch.from_numpy(x).to(device)
