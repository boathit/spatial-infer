import warnings
warnings.filterwarnings("ignore")
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import pandas as pd
import os
from toolz.curried import *
from utils import *
from torch import Tensor
from typing import Optional


class MF(nn.Module):
    def __init__(self, N: int, T: int, dim_size: int):
        super(MF, self).__init__()
        self.s_embeddings = nn.Embedding(N, dim_size)
        self.t_embeddings = nn.Embedding(T, dim_size)
        self.dims = (N, T)

    def _forward(self, roads, times):
        """
        roads (batch_size,): road link ids.
        times (batch_size,): time steps.
        """
        ## (batch_size, dim_size)
        ms = self.s_embeddings(roads)
        ## (T, dim_size)
        mt = self.t_embeddings(times)
        ## (batch_size, T)                                            
        return torch.mm(ms, mt.t())

    def forward(self, roads):
        """
        roads (batch_size, ): road link ids.
        """
        T = self.dims[1]
        times = torch.arange(T,  dtype=roads.dtype, device=roads.device)
        return self._forward(roads, times)

    def infer(self):
        """
        Infer the values of last time step.
        """
        N, T = self.dims
        device = self.get_device

        roads = torch.arange(N).to(device)
        times = torch.LongTensor([T-1]).to(device)
        return self._forward(roads, times)

    @property
    def get_device(self):
        return self.s_embeddings.weight.device

    def getU(self, roads):
        return self.s_embeddings(roads)

    def getV(self, times):
        return self.t_embeddings(times)

def obsLoss(model, X, roads):
    """
    X (N, T)
    roads (batch_size,)
    """
    ## (batch_size, T)
    x = X[roads]
    device = model.get_device

    x, roads = x.to(device), roads.to(device)
    xhat = model(roads)
    return obsLossF(xhat, x)

def spatialLoss(model: nn.Module, 
                L: torch.sparse.FloatTensor):
    """
    Laplacian constraint loss Tr(U^T L U).

    Args
        model: MF instance.
        L (N, N): Graph laplacian matrix.
    """
    N, device = model.dims[0], model.get_device
    U = model.getU(torch.arange(N).to(device))
    return torch.einsum('dn,nd->', U.T, L @ U) / N

def temporalLoss(model: nn.Module):
    T, device = model.dims[1], model.get_device
    V = model.getV(torch.arange(T).to(device))
    D = toeplitz_matrix(T, 24).to(device)
    return torch.square(D @ V).sum() / T

def L2Loss(model: nn.Module):
    N, T = model.dims
    device = model.get_device
    U = model.getU(torch.arange(N).to(device))
    V = model.getV(torch.arange(T).to(device))
    return torch.linalg.norm(U) + torch.linalg.norm(V)

def train(
    optimizer: torch.optim.Optimizer, 
    num_epochs: int, 
    batch_size: int, 
    model: nn.Module, 
    X: Tensor,
    L: torch.sparse.FloatTensor, 
    λ: float, 
    η: float,
    verbose: bool
):
    def train_step(i, roads):
        spatial_loss = spatialLoss(model, L) if i % 10 == 0 and λ > 0 else 0.0
        #temporal_loss = temporalLoss(model) if i % 10 == 0 and η > 0 else 0.0
        l2_loss = L2Loss(model) if η > 0 else 0.0
        loss = obsLoss(model, X, roads) + λ * spatial_loss + η * l2_loss

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        return loss.item()
    
    def train_epoch(epoch, idx):
        model.train()

        np.random.shuffle(idx)
        N, running_losses = len(idx), []
        for st_idx in range(0, N, batch_size):
            end_idx = min(st_idx + batch_size, N)
            roads = torch.LongTensor(idx[st_idx:end_idx])
            running_losses.append(train_step(st_idx//batch_size, roads))
        return np.mean(running_losses)
    
    idx = np.arange(X.shape[0])
    for epoch in range(num_epochs):
        running_loss = train_epoch(epoch, idx)
        if verbose and (epoch + 1) % 100 == 0:
            print(f"Epoch:{epoch:3d} running loss = {running_loss:10.4f}")
        
@torch.no_grad()
def test(model, X_unobs):
    model.eval()

    xhat = model.infer()
    x = X_unobs[:, -1:].to(xhat.device)
    mask = torch.logical_not(x.isnan())

    y_test, y_pred = x[mask], xhat[mask]
    edgeid, _ = torch.where(mask)
    return y_test.cpu().numpy(), y_pred.cpu().numpy(), edgeid.cpu().numpy()

def one_snapshot(
    X_obs: Tensor,
    X_unobs: Tensor,
    L: torch.sparse.FloatTensor,
    num_epochs: int, 
    batch_size: int, 
    dim_size: int, 
    device: torch.device,
    λ: Optional[float] = 0.0,
    η: Optional[float] = 0.0,
    verbose: Optional[bool] = False
):
    model = MF(X_obs.shape[0], X_obs.shape[1], dim_size).to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
    train(optimizer, num_epochs, batch_size, model, X_obs, L, λ, η, verbose)
    return test(model, X_unobs)

def update_obs(X_obs, X_unobs):
    """
    Merge the observation in `X_unobs` into `X_obs` except the latest time step.
    """
    observed = torch.logical_not(torch.isnan(X_unobs))
    observed[:, -1] = False
    X_obs[observed] = X_unobs[observed]
    return X_obs

np.random.seed(123)
torch.manual_seed(123)
uberdir = "/home/xiucheng/data/uber-movement/"
nykjuly = os.path.join(uberdir, "movement-speeds-hourly-new-york-2019-7.csv.zip")
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    
if __name__ == "__main__":
    nodes, edges = load_gdfs("data/newyork/")
    df = pd.read_csv(nykjuly)
    df = attach_edgeid(nodes, edges, df)
    dG = edge_topology_from_edges(edges)
    L = torch_sparse_laplacian(dG).to(device)
    λ = 1.0
    η = 1e-4
    n_test = 3 * 24
    num_epochs = 500
    batch_size = 256
    dim_size = 100

    obs, unobs = split_obs_unobs(df, ratio=0.9)
    xa_obs = dataframe2xarray(obs, edges.shape[0])
    xa_unobs = dataframe2xarray(unobs, edges.shape[0])
    X_obs, X_unobs = torch.FloatTensor(xa_obs.values), torch.FloatTensor(xa_unobs.values)
    test_stats = pd.DataFrame({})
    for i in range(n_test):
        y_test, y_pred, edgeid = one_snapshot(
            X_obs[:, :-n_test+i], 
            X_unobs[:, :-n_test+i],
            L,
            num_epochs, 
            batch_size, 
            dim_size, 
            device,
            λ=λ,
            η=η
        )

        test_stats = collect_test_stats(test_stats, i, edgeid, y_test, y_pred)
        if i % 10 == 0:
            print(f"Snapshot {i:4d}" + evaluated_message(y_test, y_pred))
    save_test_stats(test_stats, "data/exp", "nmf_test_stats" if λ == 0 else "nmf_laplacian_test_stats")
    print(evaluated_message(test_stats.y_test.values, test_stats.y_pred.values))                      
                                
                                






    