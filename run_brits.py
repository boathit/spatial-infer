from functools import partial
from pandas.core.frame import DataFrame
import torch
import numpy as np
import pandas as pd
import argparse, os
import torch.optim as optim
import util

from util import (to_device, dataframe_from_imputation)
from models import BRITS
from dataloader import BritsDataset, get_dataloader

parser = argparse.ArgumentParser()
parser.add_argument("--epochs", type=int, default=10)
parser.add_argument("--batch_size", type=int, default=200)
parser.add_argument("--rnn_hid_size", type=int, default=108)
parser.add_argument("--dataset", type=str, default="physionet")
args = parser.parse_args()

np.random.seed(0)
torch.manual_seed(0)


device = torch.device("cuda:0")

print("Loading data...")
if args.dataset == "physionet":
    dirpath = "/home/xiucheng/Github/spatial-infer/baseline/BRITS/raw/"
    df = util.load_physionet(dirpath)
    rowbound = False
    max_len = -1
    ## split into train, validation, test and do normalization
    trn, val, tst, mean, std = util.split_trn_val_tst(df, rowbound, keep_ratio=0.9)
elif args.dataset == "mdt_newyork":
    city_dirpath = "/home/xiucheng/Github/spatial-infer/data/"
    uber_dirpath = "/home/xiucheng/data/uber-movement/"
    city = util.load_mdt_geometry(os.path.join(city_dirpath, "new-york.geojson"), util.quad_longisland)
    df = pd.read_csv(os.path.join(uber_dirpath, "movement-speeds-hourly-new-york-2019-7.csv.zip"))
    df = util.transform_mdt_dataframe(df, city, max_missing_rate=0.1)
    
    ## select sids
    #sids = df.reset_index().sid.unique()
    #df = df.loc[np.random.choice(sids, 5000, replace=False)]
    ## select sids

    df.drop("speed_std", axis=1, inplace=True)
    df = util.transform_sid2feature(df)
    rowbound = False
    max_len = 124
    ## split into train, validation, test and do normalization
    trn, val, tst, mean, std = util.split_trn_val_tst(df, rowbound, keep_ratio=0.1)
else:
    raise LookupError("Unknown dataset.")

print(f"trn: {trn.shape}")
input_size = trn.shape[1]

print("Constructing dataset...")
dataset = BritsDataset(trn, val, tst, max_len)
print(f"Dataset size: {len(dataset)}")

trn_loader = get_dataloader(dataset, args.batch_size, True)
tst_loader = get_dataloader(dataset, 500, False)

model = BRITS(args.rnn_hid_size, input_size).to(device)

def train(model, trn_loader, tst_loader, epochs):
    optimizer = optim.Adam(model.parameters(), lr=1e-3)
    min_loss = 1e9
    for epoch in range(epochs):
        running_loss = 0.0
        for i, data in enumerate(trn_loader):
            data_f, data_b = to_device(device, data[1:3])
            loss, _ = model.run_on_batch(data_f, data_b, optimizer)
            running_loss += loss
            if i % 10 == 0: print(f"Running loss: {running_loss/(i+1):7.4f}")
        print(f"Epoch: {epoch:3} Loss: {running_loss/len(trn_loader):7.4f}")

        ## evaluation
        val_loss, _ = evaluate(model, tst_loader)
        if val_loss < min_loss:
            print(f"Saving model at epoch {epoch:3} evaluation loss (normalized) {val_loss:7.4f}")
            torch.save(model.state_dict(), "/tmp/best_model.pt")
            min_loss = val_loss

@torch.no_grad()
def evaluate(model, dataloader, test=False):
    model.eval()
    
    mae, n = 0.0, 0
    dfs = []
    columns = dataloader.dataset.columns

    for data in dataloader:
        (sid, tids), data_f, data_b, value_val, value_tst = data
        _, imputation = model(to_device(device, data_f), to_device(device, data_b))

        value = (value_tst if test else value_val).to(device)
        mask = torch.logical_not(torch.isnan(value))
        mae += torch.abs(imputation[mask] - value[mask]).sum().item()
        n += mask.sum().item() 

        if test:
            dfs.extend(
                ## map along the batch dimension
                map(
                    partial(dataframe_from_imputation, columns),
                    imputation.cpu().numpy(),
                    mask.cpu().numpy(),
                    sid.cpu().numpy(),
                    tids.cpu().numpy()
                )
            )
    
    model.train()
    if test:
        return mae / n, pd.concat(sorted(dfs, key=lambda df: df.index[0]))
    else:
        return mae / n, DataFrame()

def evaluated_message(obs, imp) -> None:
    mae = np.abs(obs - imp)
    
    idx = obs.abs() > 1e-5
    obs = obs[idx]
    imp = imp[idx]

    print(f"Test MAE: {mae.mean()}")
    print(f"Test MAPE: {(mae / obs.abs()).mean()}")
    print(f"Test MRE: {mae.sum() / obs.abs().sum()}")

def run(epochs: int, train_model=True) -> None:
    if train_model: train(model, trn_loader, tst_loader, epochs)
    model.load_state_dict(torch.load("/tmp/best_model.pt"))
    tst_loss, tst_imp = evaluate(model, tst_loader, test=True)
    print(f"Test loss (normalized): {tst_loss:7.4f}")

    tst_obs_norm = tst.stack()
    tst_imp_norm = tst_imp.stack()
    print("normalized error:")
    evaluated_message(tst_obs_norm, tst_imp_norm)

    tst_obs_unnorm = util.denormalize_dataframe(tst, mean, std).stack()
    tst_imp_unnorm = util.denormalize_dataframe(tst_imp, mean, std).stack()
    print("unnormalized error:")
    evaluated_message(tst_obs_unnorm, tst_imp_unnorm)

if __name__ == "__main__":
    run(args.epochs, True)