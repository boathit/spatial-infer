import numpy as np
import util
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm
from pandas import DataFrame
from typing import Tuple

class BritsDataset(Dataset):
    def __init__(self, trn: DataFrame, val: DataFrame, tst: DataFrame, max_len=-1) -> None:
        self.sids = trn.reset_index().sid.unique()
        self.tids = trn.loc[self.sids[0]].reset_index().tid.values
        self.columns = trn.columns
        self.data = []
        
        max_len = len(self.tids) if max_len <= 0 else max_len
        assert len(self.tids) % max_len == 0, f"max_len should be a factor of {len(self.tids)}"
        num_seq = len(self.tids) // max_len
        for sid in tqdm(self.sids):
            value_trn = trn.loc[sid].values.astype(np.float32)
            value_val = val.loc[sid].values.astype(np.float32)
            value_tst = tst.loc[sid].values.astype(np.float32)
            for tids, v_trn, v_val, v_tst in zip(
                np.split(self.tids, num_seq),
                np.split(value_trn, num_seq),
                np.split(value_val, num_seq),
                np.split(value_tst, num_seq) 
            ):
                self.append(sid, tids, v_trn, v_val, v_tst)
            

    def append(self, sid, tids, value_trn, value_val, value_tst) -> None:
        value_f = value_trn
        value_b = value_trn[::-1]
        mask_f, delta_f = util.construct_mask_δ(value_f)
        mask_b, delta_b = util.construct_mask_δ(value_b)

        self.data.append((
            (sid, tids),
            (np.nan_to_num(value_f), mask_f, delta_f),
            (np.nan_to_num(value_b), mask_b, delta_b),
            value_val,
            value_tst
        ))
    
    def __len__(self) -> int:
        return len(self.data)

    def __getitem__(self, index) -> Tuple:
        return self.data[index]

def get_dataloader(dataset, batch_size: int, shuffle: bool):
    return DataLoader(
        dataset=dataset,
        batch_size=batch_size,
        shuffle=shuffle
    )
   