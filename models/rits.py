import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
import math


class FeatureRegression(nn.Module):
    def __init__(self, input_size):
        super(FeatureRegression, self).__init__()
        self.build(input_size)

    def build(self, input_size):
        self.W = Parameter(torch.Tensor(input_size, input_size))
        self.b = Parameter(torch.Tensor(input_size))

        m = torch.ones(input_size, input_size) - torch.eye(input_size, input_size)
        self.register_buffer('m', m)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.W.size(0))
        self.W.data.uniform_(-stdv, stdv)
        if self.b is not None:
            self.b.data.uniform_(-stdv, stdv)

    def forward(self, x):
        z_h = F.linear(x, self.W * self.m, self.b)
        return z_h

class TemporalDecay(nn.Module):
    def __init__(self, input_size, output_size, diag = False):
        super(TemporalDecay, self).__init__()
        self.diag = diag
        self.build(input_size, output_size)

    def build(self, input_size, output_size):
        self.W = Parameter(torch.Tensor(output_size, input_size))
        self.b = Parameter(torch.Tensor(output_size))

        if self.diag == True:
            assert(input_size == output_size)
            m = torch.eye(input_size, input_size)
            self.register_buffer('m', m)

        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.W.size(0))
        self.W.data.uniform_(-stdv, stdv)
        if self.b is not None:
            self.b.data.uniform_(-stdv, stdv)

    def forward(self, d):
        if self.diag == True:
            gamma = F.relu(F.linear(d, self.W * self.m, self.b))
        else:
            gamma = F.relu(F.linear(d, self.W, self.b))
        gamma = torch.exp(-gamma)
        return gamma

class RITS(nn.Module):
    def __init__(self, rnn_hid_size, input_size):
        super(RITS, self).__init__()

        self.rnn_hid_size = rnn_hid_size
        self.input_size = input_size
        self.build(input_size)

    def build(self, input_size):
        self.rnn_cell = nn.LSTMCell(input_size * 2, self.rnn_hid_size)

        self.temp_decay_h = TemporalDecay(input_size = input_size, output_size = self.rnn_hid_size, diag = False)
        self.temp_decay_x = TemporalDecay(input_size = input_size, output_size = input_size, diag = True)
        self.hist_reg = nn.Linear(self.rnn_hid_size, input_size)
        self.feat_reg = FeatureRegression(input_size)
        self.weight_combine = nn.Linear(input_size * 2, input_size)
        self.dropout = nn.Dropout(p = 0.25)
        self.out = nn.Linear(self.rnn_hid_size, 1)

    def forward(self, data):
        """
        data (value, mask, delta)
        """
        # (batch_size, seq_len, n)
        value, mask, delta = data
        seq_len = value.shape[1]

        h = torch.zeros((value.size()[0], self.rnn_hid_size), requires_grad=True, device=value.device)
        c = torch.zeros((value.size()[0], self.rnn_hid_size), requires_grad=True, device=value.device)
        x_loss = 0.0
        imputation = []

        for t in range(seq_len):
            x = value[:, t, :]
            m = mask[:, t, :]
            d = delta[:, t, :]

            gamma_h = self.temp_decay_h(d)
            gamma_x = self.temp_decay_x(d)

            h = h * gamma_h

            x_h = self.hist_reg(h)
            x_loss += torch.sum(torch.abs(x - x_h) * m) / (torch.sum(m) + 1e-5)
            
            x_c =  m * x +  (1 - m) * x_h
            z_h = self.feat_reg(x_c)
            x_loss += torch.sum(torch.abs(x - z_h) * m) / (torch.sum(m) + 1e-5)

            alpha = self.weight_combine(torch.cat([gamma_x, m], dim = 1))
            c_h = alpha * z_h + (1 - alpha) * x_h
            x_loss += torch.sum(torch.abs(x - c_h) * m) / (torch.sum(m) + 1e-5)

            c_c = m * x + (1 - m) * c_h
            inputs = torch.cat([c_c, m], dim = 1)
            h, c = self.rnn_cell(inputs, (h, c))
            imputation.append(c_c.unsqueeze(dim = 1))

        imputation = torch.cat(imputation, dim = 1)

        # y_h = self.out(h)
        # y_loss = binary_cross_entropy_with_logits(y_h, labels, reduce = False)
        # y_loss = torch.sum(y_loss * is_train) / (torch.sum(is_train) + 1e-5)
        # y_h = F.sigmoid(y_h)

        return x_loss, imputation.detach()

    def run_on_batch(self, data, optimizer):
        loss, imputation = self.forward(data)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        return loss.item(), imputation
