import torch
import torch.nn as nn

from .rits import RITS

class BRITS(nn.Module):
    def __init__(self, rnn_hid_size, input_size):
        super(BRITS, self).__init__()

        self.rnn_hid_size = rnn_hid_size
        self.input_size = input_size
        self.build(input_size)

    def build(self, input_size):
        self.rits_f = RITS(self.rnn_hid_size, input_size)
        self.rits_b = RITS(self.rnn_hid_size, input_size)

    def forward(self, data_f, data_b):
        loss_f, imputation_f = self.rits_f(data_f)
        loss_b, imputation_b = self.rits_b(data_b)
        imputation_b = torch.flip(imputation_b, dims=[1])

        loss_c = self.get_consistency_loss(imputation_f, imputation_b)
        loss = loss_f + loss_b + loss_c
        imputation = (imputation_f + imputation_b) / 2

        return loss, imputation

    def get_consistency_loss(self, pred_f, pred_b):
        loss = torch.abs(pred_f - pred_b).mean() * 1e-1
        return loss


    def run_on_batch(self, data_f, data_b, optimizer):
        loss, imputation = self.forward(data_f, data_b)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        return loss.item(), imputation

