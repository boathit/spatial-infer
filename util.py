import pandas as pd
import numpy as np
from shapely import geometry
import xarray as xa
import geopandas as gpd
import re, os
import torch
from shapely.geometry import Polygon
from datetime import datetime
from sklearn import metrics
from tqdm import tqdm
from numba import jit

from toolz.curried import *
from typing import List, Tuple, Optional
from pandas import DataFrame, Series
from geopandas import GeoDataFrame

## long island boundary obtained from google map
longisland = [(40.704763, -74.024003), (40.753285, -74.014733),
              (40.837756, -73.958771), (40.878783, -73.929589),
              (40.859795, -73.878379), (40.792517, -73.930708),
              (40.753429, -73.954582), (40.734194, -73.968032),
              (40.707818, -73.977279), (40.704249, -73.998968),
              (40.699278, -74.018135)]

half_longisland = [
    (40.705869, -74.026406), (40.754000, -74.015420),
    (40.789354, -73.989301), (40.773496, -73.941579),
    (40.733705, -73.968702), (40.708465, -73.976598),
    (40.706643, -73.995138), (40.696232, -74.015050)
]

quad_longisland = [
    (40.700336, -74.023461), (40.756793, -74.012990),
    (40.743139, -73.964410), (40.707493, -73.977628)
]

def load_mdt_geometry(name: str, boundary: List) -> GeoDataFrame:
    """
    Load the geometry created by, `mdt create-geometry-file new_york 2019 > new-york.geojson`,
    https://www.npmjs.com/package/movement-data-toolkit.

    Usage:
        city = load_mdt_geometry("new-york.geojson")
    """
    city = gpd.read_file(name)
    city["sid"] = city.index
    city.rename(columns={"osmstartnodeid": "osm_start_node_id",
                         "osmendnodeid": "osm_end_node_id",
                         "osmwayid": "osm_way_id",
                         "osmhighway": "osm_highway",
                         "osmname": "osm_name"}, inplace=True)
    ## select and return streets inside the area
    area = Polygon([(lon, lat) for (lat, lon) in boundary])
    return city[city.geometry.map(lambda x: area.contains(x))]

def transform_mdt_dataframe(mdt_data: DataFrame, city: GeoDataFrame, max_missing_rate=0.5) -> DataFrame:
    """
    Args:
        mdt_data: Uber movement data.
        city: returned from `load_mdt_geometry`.
    Returns:
        dataframe with index ["sid", "tid"] and data ["speed_mean", "speed_std"].
    """
    cols = ["osm_start_node_id", "osm_end_node_id", "osm_way_id"]
    city = city[["sid"] + cols]
    mdt_data = pd.merge(mdt_data, city, left_on=cols, right_on=cols, how="left")
    mdt_data = mdt_data[~mdt_data.sid.isna()]
    mdt_data["sid"] = mdt_data.sid.astype(np.int)

    cols = ["year", "month", "day", "hour", "sid", "speed_mph_mean", "speed_mph_stddev"]
    mdt_data = mdt_data[cols].rename(columns={"speed_mph_mean": "speed_mean",
                                              "speed_mph_stddev": "speed_std"})
    #mdt_data["tid"] = mdt_data["tid"].map(lambda s: datetime.strptime(s, "%Y-%m-%dT%H:%M:%S.000Z"))
    #mdt_data["tid"] = mdt_data[["year", "month", "day", "hour"]].apply(lambda x: datetime(*x), axis=1)
    mdt_data["tid"] = mdt_data[["day", "hour"]].apply(lambda x: (x[0] - 1)*24 + x[1], axis=1)
    mdt_data.drop(["year", "month", "day", "hour"], axis=1, inplace=True)

    ## selected sid by max_missing_rate
    #tids = pd.date_range(start=mdt_data.tid.min(), end=mdt_data.tid.max(), freq="H")
    tids = np.arange(start=mdt_data.tid.min(), stop=mdt_data.tid.max()+1)
    min_observation = (1 - max_missing_rate) * len(tids)
    stat = (mdt_data.groupby("sid").agg(["count"]).tid >= min_observation).reset_index()
    sids = stat.sid[stat["count"]].values
    mdt_data = mdt_data[mdt_data.sid.isin(sids)]

    index = pd.MultiIndex.from_product([sids, tids], names=["sid", "tid"])
    return mdt_data.set_index(["sid", "tid"]).reindex(index)

def mdt_xarray(mdt_data: DataFrame, max_missing_rate=.5) -> xa.Dataset:
    mdt_data = mdt_data.to_xarray()
    max_missing_num = max_missing_rate * mdt_data.speed_mean.shape[1]
    index = mdt_data.speed_mean.isnull().sum(dim="tid") < max_missing_num
    return xa.merge([mdt_data.speed_mean[index], mdt_data.speed_std[index]])

def transform_physionet_dataframe(phy_data: DataFrame) -> DataFrame:
    sid = int(phy_data.Value[0])
    phy_data = phy_data.iloc[1:-1].copy()
    phy_data["sid"] = sid
    phy_data["tid"] = phy_data.Time.map(lambda t: int(re.search(r"([0-9]+):", t).group(1)))
    agg = phy_data.groupby(["sid", "tid", "Parameter"]).agg(["mean"])
    index = [(sid, tid) for tid in range(48)]
    return pd.Series(index=agg.index, data=agg.values.ravel()).unstack(level=-1).reindex(index)

def load_physionet(dirpath: str) -> DataFrame:
    ## used
    cols = [
        'DiasABP', 'HR', 'Na', 'Lactate', 'NIDiasABP', 'PaO2', 'WBC', 'pH', 'Albumin', 'ALT', 'Glucose', 'SaO2',
        'Temp', 'AST', 'Bilirubin', 'HCO3', 'BUN', 'RespRate', 'Mg', 'HCT', 'SysABP', 'FiO2', 'K', 'GCS',
        'Cholesterol', 'NISysABP', 'TroponinT', 'MAP', 'TroponinI', 'PaCO2', 'Platelets', 'Urine', 'NIMAP',
        'Creatinine', 'ALP'
    ]
    phy_df = pd.DataFrame()
    for file in tqdm(os.listdir(dirpath)):
        if re.search("\d{6}", file) is not None:
            phy_data = pd.read_csv(os.path.join(dirpath, file))
            phy_df = pd.concat([
                phy_df, 
                transform_physionet_dataframe(phy_data)
            ])
    return phy_df[sorted(cols)]

def normalize_dataframe(df: DataFrame, mean=None, std=None) -> DataFrame:
    if mean is None or std is None:
        mean, std = df.mean(), df.std()
    return (df - mean) / std, mean, std

def denormalize_dataframe(df: DataFrame, mean: Series, std: Series) -> DataFrame:
    return df * std + mean

def split_obs_matrix(x: np.array, rowbound: bool, keep_ratio: float) -> Tuple[np.array, np.array]:
    """
    Making two copies of `x`, where `1 - keep_ratio` and `keep_ratio` of observed entries of `x` 
    are set to nan in the first copy and second copy, respectively. If `rowbound` is true,
    then set the corresponding entire rows together. 
    """
    x = x.copy()
    y = x.copy()
    if rowbound is False:
        idx0, idx1 = np.where(np.logical_not(np.isnan(x)))
        mask_x = np.random.rand(len(idx0)) > keep_ratio
        mask_y = np.logical_not(mask_x)
        x[idx0[mask_x], idx1[mask_x]] = np.nan
        y[idx0[mask_y], idx1[mask_y]] = np.nan
    else:
        idx, = np.where(np.logical_not(np.isnan(x[:, 0])))
        mask_x = np.random.rand(len(idx)) > keep_ratio
        mask_y = np.logical_not(mask_x)
        x[idx[mask_x], :] = np.nan
        y[idx[mask_y], :] = np.nan
    return x, y

def split_obs_dataframe(df: DataFrame, rowbound: bool, keep_ratio: float) -> Tuple[DataFrame, DataFrame]:
    """
    Analogous to `split_obs_matrix` but operating on dataframe.
    """
    x = df.values
    x, y = split_obs_matrix(x, rowbound, keep_ratio)
    return (DataFrame(data=x, index=df.index, columns=df.columns), 
            DataFrame(data=y, index=df.index, columns=df.columns))

def split_trn_val_tst(df: DataFrame, rowbound: bool, keep_ratio=.8):
    """
    Split `df` into trn, val, tst and normalize all using mean, std of trn_val.
    """
    trn_val, tst = split_obs_dataframe(df, rowbound, keep_ratio)
    trn_val, mean, std = normalize_dataframe(trn_val)
    tst, _, _ = normalize_dataframe(tst, mean, std)
    trn, val = split_obs_dataframe(trn_val, rowbound, 0.9)
    return trn, val, tst, mean, std

def collect_entries(x_truth: np.array, x_pred: np.array) -> DataFrame:
    """
    Collect the entries of `x_truth` and `x_pred` where `x_truth` is not nan.
    """
    idx0, idx1 = np.where(np.logical_not(np.isnan(x_truth)))
    y_truth, y_pred = x_truth[idx0, idx1], x_pred[idx0, idx1]
    return DataFrame(
        {
            "sid": idx0,
            "tid": idx1,
            "y_truth": y_truth,
            "y_pred": y_pred
        }
    )

def dataframe_from_imputation(
    columns,
    data: np.array,
    mask: np.array,
    sid: int,
    tids: np.array
) -> DataFrame:
    """
    DataFrame indexed by (sid, tids), set the data[~mask] to nan.

    Args:
        data: the imputed data matrix.
        mask: the boolen matrix indicated where have observations.
    """
    data[~mask] = np.nan
    return DataFrame(
        data,
        index=pd.MultiIndex.from_product([[sid], tids], names=["sid", "tid"]),
        columns=columns
    )

def transform_sid2feature(df: DataFrame) -> DataFrame:
    sids = df.reset_index().sid.unique()
    tids = df.loc[sids[0]].index
    data = df.values.reshape(len(sids), -1).T
    return pd.DataFrame(
        data,
        index=pd.MultiIndex.from_product([[0], tids], names=["sid", "tid"]),
        columns=sids
    )

def evaluated_message(stat: DataFrame) -> str:
    y_truth, y_pred = stat.y_truth, stat.y_pred
    rmse = metrics.mean_squared_error(y_truth, y_pred, squared=False)
    mae  = metrics.mean_absolute_error(y_truth, y_pred)
    abs_errors = np.abs(y_truth - y_pred)
    mape = np.mean(abs_errors / y_truth)
    msg = (f" RMSE: {rmse:7.4} MAE: {mae:7.4}"
           f" Max Error: {np.max(abs_errors):7.4} MAEP: {mape:7.4}")
    return msg

def to_device(device, xs):
    if torch.is_tensor(xs):
        return xs.to(device)
    elif isinstance(xs, list) or isinstance(xs, tuple):
        return list(map(partial(to_device, device), xs))
    else:
        return xs

@jit(nopython=True)
def construct_mask_δ(x: np.array) -> np.array:
    m = np.logical_not(np.isnan(x)).astype(np.float32)
    δ = np.ones_like(m)
    for i in range(1, x.shape[0]):
        δ[i, :] = 1.0 + (1 - m[i-1, :]) * δ[i-1, :]
    return m, δ


